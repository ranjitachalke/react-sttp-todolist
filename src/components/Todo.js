import React, { useState } from "react";
import { Input, Button } from "reactstrap";
import TodoList from "./TodoList";
import { todoInputPlaceHolderMessage, addTodoButtonText } from "../Constants";
import style from "../styles/Todo.module.css";

function Todo() {
  const [todoText, setTodoText] = useState("");
  const [todos, setTodos] = useState([]);

  let onTextChange = (event) => {
    setTodoText(event.target.value);
  };

  let addTodo = () => {
    if (todoText) { //Condition check so that todo should not be cretated even if no text
      setTodos((prevTodos) => [
        ...prevTodos,
        {
          message: todoText,
          isChecked: false,
        },
      ]);
      setTodoText("");
    }
    else 
    <div></div>
  };

  return (
    <div className={style["todo-container"]}>
      <div className={style["todo-input-actions"]}>
        <Input
          type="text"
          name="todo-input-text"
          placeholder={todoInputPlaceHolderMessage}
          value={todoText}
          onChange={onTextChange}
        />
        <Button
          className={style["add-todo-button"]}
          color="success"
          size="sm"
          onClick={addTodo}
        >
          {addTodoButtonText}
        </Button>
      </div>
      <TodoList todos={todos} setTodos={setTodos} />
    </div>
  );
}

export default Todo;
